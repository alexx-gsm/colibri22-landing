import { createMuiTheme } from '@material-ui/core/styles'
import { blueGrey } from '@material-ui/core/colors'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#444',
      contrastText: '#fff',
    },
    secondary: {
      main: '#ff5722',
      contrastText: '#fff',
    },
    extraColor: blueGrey,
  },
  typography: {
    useNextVariants: true,
  },
})
