import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import TopPanel from './components/TopPanel';
import Slider from './components/Slider';
import theme from './theme';
import Message from './components/Message';
// import Complex from './components/Complex';
import Promo from './components/Promo';
import Menu from './components/Menu';
import Discounts from './components/Discounts';
import Map from './components/Map';
import Footer from './components/Footer';
import './App.css';
import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <TopPanel />
      <Slider />
      <Message />
      {/* <Promo /> */}
      {/* <Complex /> */}
      <Menu />
      <Discounts />
      <Map />
      <Footer />
    </MuiThemeProvider>
  );
}

export default App;
