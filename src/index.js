import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { resolvers, typeDefs } from './graphql';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from '@apollo/react-hooks';
import * as serviceWorker from './serviceWorker';
import './index.css';

const cache = new InMemoryCache();

const appLocalStorage = localStorage.getItem('VKDALocalAppState');
const tokenObj = appLocalStorage ? JSON.parse(appLocalStorage) : {};

const link = new HttpLink({
  headers: { authorization: tokenObj.token || '' },
  uri: '/graphql',
});

const client = new ApolloClient({
  cache,
  link,
  typeDefs,
  resolvers,
});

cache.writeData({
  data: {
    isLoggedIn: false,
    user: null,
  },
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
