import gql from 'graphql-tag';
import { DISH_FIELDS } from '../fragments/dishFields';

export const GET_DISHES = gql`
  query GetDishes {
    dishes {
      isSuccess
      data {
        _id
        ...dishFields
      }
      errors {
        label
        value
      }
      code
      message
    }
  }
  ${DISH_FIELDS}
`;
