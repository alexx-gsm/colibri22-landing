import gql from 'graphql-tag';

export const CHECK_TOKEN = gql`
  query CheckToken($token: String!) {
    checkToken(input: $token) {
      email
      name
      role
      acronym
    }
  }
`;
