import gql from 'graphql-tag';
import { CATEGORY_FIELDS } from '../fragments/categoryFields';

export const GET_CATEGORIES = gql`
  query GetCategories {
    categories {
      isSuccess
      data {
        _id
        ...CategoryFields
      }
      errors {
        label
        value
      }
      code
      message
    }
  }
  ${CATEGORY_FIELDS}
`;
