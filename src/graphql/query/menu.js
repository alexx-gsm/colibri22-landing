import gql from 'graphql-tag';
import { MENU_DISH_FIELDS } from '../fragments/menuDishFields';

export const GET_MENUS = gql`
  query GetMenus {
    menusFromNow {
      isSuccess
      data {
        _id
        date
        dishes {
          _id
          ...menuDishFields
        }
      }
      message
      errors {
        label
        value
      }
      code
    }
  }
  ${MENU_DISH_FIELDS}
`;
