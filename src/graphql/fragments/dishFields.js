import gql from 'graphql-tag';

export const DISH_FIELDS = gql`
  fragment dishFields on Dish {
    title
    category {
      _id
      title
      sorting
    }
    weight
    price
    composition
    comment
  }
`;
