import gql from 'graphql-tag';
import { DISH_FIELDS } from '../../fragments/dishFields';

export const UPDATE_MENU = gql`
  mutation updateMenu(
    $_id: ID
    $date: String!
    $dishes: [MenuDishDataInput]!
    $isActive: Boolean!
  ) {
    updateMenu(
      input: { _id: $_id, date: $date, dishes: $dishes, isActive: $isActive }
    ) {
      isSuccess
      data {
        _id
        date
        isActive
        dishes {
          _id
          ...dishFields
        }
      }
      message
      errors {
        label
        value
      }
    }
  }
  ${DISH_FIELDS}
`;
