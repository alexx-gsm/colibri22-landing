import gql from 'graphql-tag';
import { DISH_FIELDS } from '../../fragments/dishFields';

export const UPDATE_DISH = gql`
  mutation updateDish(
    $_id: ID
    $title: String!
    $category: String!
    $weight: String!
    $price: String!
    $composition: String
    $comment: String
  ) {
    updateDish(
      input: {
        _id: $_id
        title: $title
        category: $category
        weight: $weight
        price: $price
        composition: $composition
        comment: $comment
      }
    ) {
      isSuccess
      data {
        _id
        ...dishFields
      }
      message
      errors {
        label
        value
      }
    }
  }
  ${DISH_FIELDS}
`;
