import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../config';
import moment from 'moment';

const MODULE_NAME = `${APP_NAME}/MENU`;

const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const FILTER_SET = `${MODULE_NAME}/FILTER_SET`;
const FILTER_CLEAR = `${MODULE_NAME}/FILTER_CLEAR`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
const SET_VALUE = `${MODULE_NAME}/SET_VALUE`;

const MainPageContext = React.createContext();

const emptyMenus = [];
const emptyFilter = {
  week: moment().isoWeek(),
};

const ReducerRecord = Record({
  menus: emptyMenus,
  filter: emptyFilter,
});

function menuPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case UPDATE_RECORD: {
      return state.setIn(['menu', payload.key], payload.value);
    }
    case FILTER_SET: {
      return state.setIn(['filter', payload.key], payload.value);
    }
    case FILTER_CLEAR: {
      return state.set('filter', emptyFilter);
    }
    case SET_ITEM: {
      return state.set('menu', payload.menu).set('isOpenDialog', true);
    }
    case SET_VALUE: {
      return state.setIn(['menu', payload.key], payload.value);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 * provider
 */
function MenuPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    menuPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <MainPageContext.Provider value={value} {...props} />;
}

/**
 * context
 */
function useMainPageContext() {
  const context = React.useContext(MainPageContext);
  if (!context) {
    throw new Error(
      `useMainPageContext must be used within a MenuPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateMenu = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setFilter = (key) => (value) =>
    dispatch({ type: FILTER_SET, payload: { key, value } });
  const clearFilter = () => dispatch({ type: FILTER_CLEAR });
  const storeMenu = (menu) => dispatch({ type: SET_ITEM, payload: { menu } });
  const setValue = (key) => (value) =>
    dispatch({ type: SET_VALUE, payload: { key, value } });

  return {
    state,
    openDialog,
    closeDialog,
    updateMenu,
    setFilter,
    clearFilter,
    storeMenu,
    setValue,
  };
}

export { MenuPageProvider, useMainPageContext };
