import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
  BoxWrap: {
    padding: theme.spacing(4),
    background: theme.palette.primary.dark,
    [theme.breakpoints.down('xs')]: {
      height: '100%',
      boxSizing: 'border-box',
      paddingTop: theme.spacing(8),
      paddingLeft: 0,
      paddingRight: 0,
      paddingBottom: theme.spacing(2),
    },
  },
  FooterContainer: {
    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
  },
  LogoContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  BoxFooterText: {
    [theme.breakpoints.down('xs')]: {
      paddingTop: theme.spacing(4),
    },
  },
  FooterLogo: {
    color: 'white',
    fontWeight: 500,
    marginBottom: theme.spacing(3)
  },
  FooterTitle: {
    color: theme.palette.common.white,
    paddingBottom: theme.spacing(2),
  },
  FooterTitleColumn: {
    [theme.breakpoints.down('xs')]: {
      '&:not(:last-child)': {
        borderBottom: `1px solid ${theme.palette.grey[800]}`,
      },
    },
  },
  FooterText: {
    color: theme.palette.grey[500],
    paddingBottom: theme.spacing(1),
  },
  TypoLink: {
    color: theme.palette.grey[600],
    paddingTop: theme.spacing(4),
    '& a': {
      color: theme.palette.secondary.main,
      textDecoration: 'none',
    },
    [theme.breakpoints.down('xs')]: {
      marginTop: 'auto',
    },
  },
}))
