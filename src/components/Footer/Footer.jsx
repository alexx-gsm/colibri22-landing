import React from "react"
import { Box, Container, Typography, Grid } from "@material-ui/core"
// styles
import useStyles from "./styles"

function Footer() {
  const classes = useStyles()
  return (
    <Box className={classes.BoxWrap}>
      <Container maxWidth="lg" className={classes.FooterContainer}>
        <Box className={classes.LogoContainer}>
          <Box className={classes.LogoWrap}>
            <Typography variant='h4' className={classes.FooterLogo}>Чайхана "Учкудук"</Typography>
          </Box>
        </Box>
        <Box className={classes.BoxFooterText}>
          <Grid container spacing={4} className={classes.FooterTitleContainer}>
            <Grid item xs={12} sm={6} className={classes.FooterTitleColumn}>
              <Typography variant="subtitle2" className={classes.FooterText}>
                - Сроки годности блюд: не более 4 часов с момента доставки (при
                температуре хранения от 20 C до 50 С).
              </Typography>
              <Typography variant="subtitle2" className={classes.FooterText}>
                - Полный состав обеда, ингредиенты и условия для бесплатной
                доставки уточняйте у оператора.
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} className={classes.FooterTitleColumn}>
              <Typography variant="subtitle2" className={classes.FooterText}>
                - Вся представленная на сайте информация носит информационный
                характер и не является публичной офертой, определяемой
                положениями ст. 437 (2) ГК РФ.
              </Typography>
              <Typography variant="subtitle2" className={classes.FooterText}>
                - Информация может быть изменена в любое время без
                предварительного уведомления.
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Typography
          variant="subtitle1"
          className={classes.TypoLink}
          align="center"
        >
          Разработка и создание сайта:{" "}
          <a
            href="http://www.instagram.com/alexx_gsm"
            target="_blank"
            rel="noopener noreferrer"
          >
            @alexx_gsm
          </a>
        </Typography>
      </Container>
    </Box>
  )
}

export default Footer
