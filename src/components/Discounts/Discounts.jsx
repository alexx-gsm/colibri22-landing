import React from "react"
// @material-ui components
import { Container, Grid, Box } from "@material-ui/core"
import { Typography } from "@material-ui/core"
import { Card, CardActionArea } from "@material-ui/core"
// styles
import useStyles from "./styles"

function Discounts() {
  const classes = useStyles()
  return (
    <Container maxWidth="lg" className={classes.Container}>
      <Typography
        variant="h2"
        className={classes.Title}
        align="center"
        color="primary"
      >
        Скидки
      </Typography>

      <Grid container alignItems="center" spacing={4}>
        <Grid item xs={12} sm={12} md={4} className={classes.GridCard}>
          <CardActionArea>
            <Card className={classes.Card}>
              <Box className={classes.CardImage}>
                <img
                  src="assets/images/discount/discount-1.jpg"
                  alt="заказ на завтра"
                  className={classes.Image}
                />
              </Box>
              <Typography
                variant="h4"
                align="center"
                className={classes.CardTitle}
              >
                "Завтра"
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                закажи обед на завтра
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                и получи скидку
              </Typography>
              <Typography
                variant="h2"
                align="center"
                className={classes.CardDiscount}
              >
                5%
              </Typography>
            </Card>
          </CardActionArea>
        </Grid>

        <Grid item xs={12} sm={12} md={4} className={classes.GridCard}>
          <CardActionArea>
            <Card className={classes.Card}>
              <Box className={classes.CardImage}>
                <img
                  src="assets/images/discount/discount-2.jpg"
                  alt="заказ на завтра"
                  className={classes.Image}
                />
              </Box>
              <Typography
                variant="h4"
                align="center"
                className={classes.CardTitle}
              >
                "Неделя"
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                закажи обед на неделю (5 дней)
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                и получи скидку
              </Typography>
              <Typography
                variant="h2"
                align="center"
                className={classes.CardDiscount}
              >
                10%
              </Typography>
            </Card>
          </CardActionArea>
        </Grid>

        <Grid item xs={12} sm={12} md={4} className={classes.GridCard}>
          <CardActionArea>
            <Card className={classes.Card}>
              <Box className={classes.CardImage}>
                <img
                  src="assets/images/discount/discount-3.jpg"
                  alt="заказ на завтра"
                  className={classes.Image}
                />
              </Box>
              <Typography
                variant="h4"
                align="center"
                className={classes.CardTitle}
              >
                "Друзья"
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                расскажи Друзьям о нас
              </Typography>
              <Typography
                variant="subtitle1"
                align="center"
                className={classes.CardSubTitle}
              >
                и получай кешбэк с каждого их заказа
              </Typography>
              <Typography
                variant="h2"
                align="center"
                className={classes.CardDiscount}
              >
                5%
              </Typography>
            </Card>
          </CardActionArea>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Discounts
