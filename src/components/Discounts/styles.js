import { makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => ({
  Container: {
    paddingBottom: theme.spacing(10),
    paddingLeft: 0,
    paddingRight: 0,
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
    [theme.breakpoints.down("xs")]: {
      paddingBottom: theme.spacing(2),
    },
  },
  GridCard: {
    [theme.breakpoints.down("sm")]: {
      maxWidth: "480px",
      marginLeft: "auto",
      marginRight: "auto",
      "&:not(:last-child)": {
        paddingBottom: 0,
      },
    },
  },
  Card: {
    position: "relative",
    paddingTop: "66.66%",
  },
  CardImage: {
    position: "absolute",
    top: 0,
    left: 0,
  },
  Image: {
    maxWidth: "100%",
  },
  Title: {
    padding: `${theme.spacing(4)}px 0`,
    fontFamily: '"Philosopher", sans-serif',
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.down("xs")]: {
      fontSize: "2.2rem",
    },
  },
  CardTitle: {
    background: theme.palette.secondary.main,
    color: theme.palette.common.white,
    textTransform: "uppercase",
    fontSize: "2rem",
    marginBottom: theme.spacing(2),
  },
  CardSubTitle: {
    lineHeight: 1,
  },
  CardDiscount: {
    padding: theme.spacing(2),
    color: theme.palette.secondary.dark,
  },
}))
