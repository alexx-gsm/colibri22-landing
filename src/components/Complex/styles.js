import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
  Container: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),

    [theme.breakpoints.down('sm')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  Title: {
    marginLeft: theme.spacing(2),
    fontWeight: theme.typography.fontWeightMedium,
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.5rem',
      textAlign: 'center',
      margin: theme.spacing(3, 0),
    },
  },

  Card: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      borderRadius: 0,
      boxShadow: 'none',
    },
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },

  CardImage: {
    flex: 2,
    [theme.breakpoints.down('xs')]: {
      height: 200,
      flex: 'none',
    },
  },

  CardContent: {
    flex: 3,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
}))
