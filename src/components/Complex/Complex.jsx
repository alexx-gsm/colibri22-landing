import React from 'react';
import { Card, CardContent, CardMedia } from '@material-ui/core';
import { Container, Typography } from '@material-ui/core';
import { List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import useStyles from './styles';

function Complex() {
  const classes = useStyles();

  return (
    <Container maxWidth='md' className={classes.Container}>
      <Card className={classes.Card}>
        <CardMedia
          className={classes.CardImage}
          image='assets/images/complex/20201002_134026.jpg'
          title='Live from space album cover'
        />
        <CardContent className={classes.CardContent}>
          <Typography variant='h3' classes={{ root: classes.Title }}>
            Комплексный обед
          </Typography>

          <List dense>
            <ListItem>
              <ListItemIcon>
                <DoneIcon fontSize='small' color='secondary' />
              </ListItemIcon>
              <ListItemText primary='Стейк лосося 100г' />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <DoneIcon fontSize='small' color='secondary' />
              </ListItemIcon>
              <ListItemText
                primary='Рис цветной 100г'
                secondary='(горошек зеленый, кукуруза консерв., перец болгарский)'
              />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <DoneIcon fontSize='small' color='secondary' />
              </ListItemIcon>
              <ListItemText
                primary='Салат овощной 80г'
                secondary='(лист салата, огурец, помидор)'
              />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <DoneIcon fontSize='small' color='secondary' />
              </ListItemIcon>
              <ListItemText
                primary='Соус 50г'
                secondary='(соевый соус, масло оливковое)'
              />
            </ListItem>
          </List>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Complex;
