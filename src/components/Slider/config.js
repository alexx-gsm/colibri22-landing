export default {
  auto: true,
  fullWidth: true,
  width: '',
  interval: 5000,
  images: [
    {
      id: '1',
      title: 'Обеды с доставкой',
      subtitle: 'в офис, на дом',
      link: '',
      imgSrc: 'image-1.jpg',
    },
    {
      id: '2',
      title: 'Постоянным клиентам скидки',
      subtitle: 'при заказе на завтра',
      link: '',
      imgSrc: 'image-2.jpg',
    },
    {
      id: '3',
      title: 'Обеды с доставкой',
      subtitle: 'в офис, на дом',
      link: '',
      imgSrc: 'image-3.jpg',
    },
    {
      id: '4',
      title: 'Постоянным клиентам скидки',
      subtitle: 'при заказе на завтра',
      link: '',
      imgSrc: 'image-4.jpg',
    },
  ],
}
