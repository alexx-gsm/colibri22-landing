import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
  SliderWrap: {
    position: 'relative',
  },
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 255,
    display: 'block',
    maxWidth: 400,
    overflow: 'hidden',
    width: '100%',
  },
  ImageWrap: {
    paddingTop: '30%',
    position: 'relative',
    [theme.breakpoints.down('lg')]: {
      paddingTop: '40%',
    },
    [theme.breakpoints.down('md')]: {
      paddingTop: 0,
      height: '50vmax',
    },
    [theme.breakpoints.down('xs')]: {
      paddingTop: 0,
      height: '100vmax',
    },
  },
  Image: {
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  ImageGradient: {
    width: '100%',
    height: '100%',
    background: 'rgba(0, 0, 0, 0.5)',
  },
  InfoContainerRoot: { height: '100%' },
  InfoWrap: {
    width: '50%',
    height: '100%',
    // padding: theme.spacing(2),
    margin: 0,
    [theme.breakpoints.down('md')]: {
      width: '65%',
    },
    [theme.breakpoints.down('xs')]: {
      width: '80%',
    },
  },
  InfoTitle: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.common.white,
    paddingBottom: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      fontSize: '3rem',
    },
  },
  InfoSubTitle: {
    color: theme.palette.common.white,
    paddingBottom: theme.spacing(6),
    [theme.breakpoints.down('sm')]: {
      fontSize: '2rem',
    },
  },
  StepperWrap: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      bottom: '50px',
    },
  },
  StepperRoot: {
    background: 'transparent',
    minWidth: '400px',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      minWidth: '200px',
    },
  },
  StepperDot: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
  StepperActiveDot: {
    backgroundColor: theme.palette.secondary.main,
  },
  StepperHandler: {
    color: 'rgba(255, 255, 255, 0.4)',
  },
}))