import React, { useState } from 'react'
import SwipeableViews from 'react-swipeable-views'
import { autoPlay } from 'react-swipeable-views-utils'
// @material-ui components
import { Grid, Box, Typography, Button, Container } from '@material-ui/core'
import { Grow, MobileStepper, IconButton } from '@material-ui/core'
// @material-ui icons
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
//config
import config from './config'
// styles
import useStyles from './styles'

const AutoPlaySwipeableViews = autoPlay(SwipeableViews)

function Slider() {
  const classes = useStyles()
  const {images} = config
  const [activeStep, setActiveStep] = useState(0)
  const [isShown, setIsShown] = useState(true)
  const maxSteps = images.length

  const handleStepChange = (step) => {
    setActiveStep(step)
    setIsShown(false)
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    setIsShown(false)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
    setIsShown(false)
  }

  return (
    <Box className={classes.SliderWrap}>
      <AutoPlaySwipeableViews
        axis={'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
        interval={config.interval}
        onTransitionEnd={() => setIsShown(true)}
        // autoplay={false}
      >
        {images.map((image, index) => (
          <Box key={image.id} className={classes.ImageWrap}>
            {Math.abs(activeStep - index) <= 2 ? (
              <div
                className={classes.Image}
                style={{
                  backgroundImage: `url(assets/images/${image.imgSrc})`,
                }}
              >
                <Box className={classes.ImageGradient}>
                  <Container
                    maxWidth="lg"
                    classes={{ root: classes.InfoContainerRoot }}
                  >
                    <Grid
                      container
                      direction="column"
                      justify="center"
                      alignItems="flex-start"
                      // spacing={3}
                      className={classes.InfoWrap}
                    >
                      <Grid item>
                        <Grow in={isShown}>
                          <Typography
                            variant="h2"
                            className={classes.InfoTitle}
                          >
                            {image.title}
                          </Typography>
                        </Grow>
                      </Grid>
                      <Grid item>
                        <Grow
                          in={isShown}
                          style={{ transformOrigin: '0 0 0' }}
                          {...(isShown ? { timeout: 1000 } : {})}
                        >
                          <Typography
                            variant="h4"
                            className={classes.InfoSubTitle}
                          >
                            {image.subtitle}
                          </Typography>
                        </Grow>
                      </Grid>
                      {/*<Grid item>*/}
                      {/*  <Grow*/}
                      {/*    in={isShown}*/}
                      {/*    style={{ transformOrigin: '0 0 0' }}*/}
                      {/*    {...(isShown ? { timeout: 1500 } : {})}*/}
                      {/*  >*/}
                      {/*    <Button*/}
                      {/*      variant="contained"*/}
                      {/*      color="secondary"*/}
                      {/*      size="large"*/}
                      {/*    >*/}
                      {/*      Подробнее ...*/}
                      {/*    </Button>*/}
                      {/*  </Grow>*/}
                      {/*</Grid>*/}
                    </Grid>
                  </Container>
                </Box>
              </div>
            ) : null}
          </Box>
        ))}
      </AutoPlaySwipeableViews>

      <Box className={classes.StepperWrap}>
        <MobileStepper
          variant="dots"
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          className={classes.StepperRoot}
          classes={{
            dot: classes.StepperDot,
            dotActive: classes.StepperActiveDot,
          }}
          nextButton={
            <IconButton
              onClick={handleNext}
              disabled={activeStep === maxSteps - 1}
              className={classes.StepperHandler}
            >
              <KeyboardArrowRight fontSize="large" />
            </IconButton>
          }
          backButton={
            <IconButton
              onClick={handleBack}
              disabled={activeStep === 0}
              className={classes.StepperHandler}
            >
              <KeyboardArrowLeft fontSize="large" />
            </IconButton>
          }
        />
      </Box>
    </Box>
  )
}

export default Slider
