import React from 'react';
import { Container, Paper, Typography } from '@material-ui/core';
import useStyles from './styles';
import formatter from '../../utils/formatter';

const MIN_ORDER = 200;
const DELIVERY = 100;

function Message() {
  const classes = useStyles();

  return (
    <Container maxWidth='sm'>
      <Paper square variant='outlined' className={classes.PaperInfo}>
        <Typography variant='body1' align='center'>
          Бесплатная доставка при заказе от
        </Typography>
        <Typography variant='h2' align='center'>
          {formatter.format(MIN_ORDER)}
        </Typography>
        <Typography variant='body1' align='center'>
          с адреса (организации, офиса).
        </Typography>
        <Typography variant='h6' align='center'>
          {
          `При заказе менее ${formatMoney(MIN_ORDER)}, доставка ${formatMoney(DELIVERY)}`}
        </Typography>
      </Paper>
    </Container>
  );

  function formatMoney(amount) {
    return formatter.format(amount);
  }
}

export default Message;
