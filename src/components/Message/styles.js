import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
  PaperInfo: {
    marginTop: theme.spacing(4),
    padding: theme.spacing(2),
    background: theme.palette.grey[100],
  },
}))
