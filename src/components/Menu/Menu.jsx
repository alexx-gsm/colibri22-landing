import React from 'react';
import { Container, Grid, Paper, Typography } from '@material-ui/core';
import { AppBar, Tabs, Tab, Box } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { categories } from './categories';
import SwipeableViews from 'react-swipeable-views';

import { useQuery } from '@apollo/react-hooks';
import { GET_MENUS } from '../../graphql/query';

import formatter from '../../utils/formatter';
import moment from 'moment';
import useStyles from './styles';

// const startTab = moment().hour() > 10 ? 1 : 0;
// const isDisableTodayMenu = moment().hour() > 14;

function Menu() {
  const classes = useStyles();

  const { data, loading } = useQuery(GET_MENUS, {
    fetchPolicy: 'no-cache'
  });
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  console.log('data :>> ', data);

  return (
    <Container maxWidth='sm' className={classes.Container}>
      {loading ? <Skeleton /> : getMenusSection()}
      {getInfoBlock()}
    </Container>
  );

  function getMenusSection() {
    const { isSuccess, data: menus } = data.menusFromNow;

    if (!isSuccess || !menus.length) {
      return '';
    }

    return (
      <>
        {getTitle('Меню')}
        <AppBar position='static' color='default'>
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor='secondary'
            textColor='secondary'
            aria-label='full width tabs example'
            variant='fullWidth'
            centered
          >
            {menus.map(({ _id, date }) => (
              <Tab
                key={_id}
                id={`tab-${_id}`}
                label={getTabLabel(date)}
                aria-controls={`tabpanel-${_id}`}
              />
            ))}
          </Tabs>
        </AppBar>
        <SwipeableViews index={value} onChangeIndex={handleChangeIndex}>
          {menus.map(getTabPanel)}
        </SwipeableViews>
      </>
    );
  }

  function getTabLabel(date) {
    return (
      <Grid container direction='column' alignItems='center'>
        <Grid item>
          <Typography variant='h5'>{moment(date).format('dddd')}</Typography>
        </Grid>
        <Grid item>
          <Typography variant='caption'>
            {moment(date).format('D MMMM')}
          </Typography>
        </Grid>
      </Grid>
    );
  }

  function getTabPanel({ _id, dishes }, index) {
    return (
      <Box
        key={_id}
        id={`tabpanel-${_id}`}
        hidden={value !== index}
        aria-labelledby={`tab-${_id}`}
      >
        {getMenuBlock(dishes)}
      </Box>
    );
  }

  function getTitle(title) {
    return (
      <Typography
        variant='h2'
        className={classes.Title}
        align='center'
        color='primary'
      >
        {title}*
      </Typography>
    );
  }

  function getMenuBlock(dishes) {
    return (
      <Paper square classes={{ root: classes.Paper }}>
        {categories.map((category) => {
          const categorisedDishes = dishes.filter(
            (cd) => cd.category.title === category,
          );
          return categorisedDishesBlock(category, categorisedDishes);
        })}
      </Paper>
    );
  }

  function categorisedDishesBlock(category, dishes) {
    if (!dishes.length) return '';

    console.log('category :>> ', category);
    console.log('dishes :>> ', dishes);

    return (
      <React.Fragment key={category}>
        <Typography
          variant='caption'
          align='center'
          className={classes.CatTitle}
        >
          {category}
        </Typography>
        {dishes.map((cd) => {
          return (
            <Grid
              container
              spacing={1}
              alignItems='flex-end'
              wrap='nowrap'
              key={cd._id}
            >
              <Grid item xs={1} className={classes.GridIndex}>
                <Typography>{cd.index}.</Typography>
              </Grid>
              <Grid item className={classes.GridFoodTitle}>
                <Typography align='right' className={classes.FoodTitle}>
                  {cd.title}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  align='right'
                  className={classes.Weight}
                  variant='caption'
                >
                  {cd.weight}
                </Typography>
              </Grid>
              <Grid item xs>
                <Typography align='right' className={classes.Price}>
                  {formatter.format(cd.price)}
                </Typography>
              </Grid>
            </Grid>
          );
        })}
      </React.Fragment>
    );
  }

  function getInfoBlock() {
    return (
      <Paper square variant='outlined' className={classes.PaperInfo}>
        <Typography variant='body1' align='center'>
          *Прием заказов:
        </Typography>
        <Typography variant='body2' align='center'>
          ПН - ПТ
        </Typography>
        <Typography variant='h4' align='center'>
          8:00 - 10:00
        </Typography>
        <Typography
          variant='body1'
          align='center'
          className={classes.TypoPhone}
        >
          по телефону: <span>8-983-170-0471</span>
        </Typography>
        <Typography
          variant='body1'
          align='center'
          className={classes.TypoPhone}
        >
          whatsApp: <span>8-964-086-8880</span>
        </Typography>
      </Paper>
    );
  }
}

export default Menu;
