import { makeStyles } from '@material-ui/core/styles';
import { deepOrange } from '@material-ui/core/colors';

export default makeStyles((theme) => ({
  Container: {
    paddingBottom: theme.spacing(10),
    paddingLeft: 0,
    paddingRight: 0,
    [theme.breakpoints.down('md')]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    },
    [theme.breakpoints.down('xs')]: {
      paddingBottom: theme.spacing(2),
    },
  },
  Title: {
    marginTop: theme.spacing(4),
    fontFamily: '"Philosopher", sans-serif',
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.down('xs')]: {
      fontSize: '2.2rem',
    },
  },
  Paper: {
    padding: theme.spacing(2, 1, 3, 1),
    marginTop: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      // padding: 0,
      boxShadow: 'none',
    },
  },
  CatTitle: {
    display: 'block',
    textTransform: 'uppercase',
    color: deepOrange[800],
    marginTop: theme.spacing(2),
  },
  GridIndex: {
    flexShrink: 0,
    alignSelf: 'center',
  },
  Weight: {
    fontSize: '0.8rem',
    fontWeight: theme.typography.fontWeightLight,
  },
  GridFoodTitle: {
    overflow: 'hidden',
  },
  FoodTitle: {
    fontSize: '1.1rem',
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.grey[900],
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  Desc: {
    fontSize: '0.8rem',
  },
  Price: {
    borderBottom: `1px dotted ${theme.palette.divider}`,
  },
  PaperInfo: {
    marginTop: theme.spacing(2),
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: theme.spacing(2),
    maxWidth: '300px',
    background: theme.palette.grey[200],
  },
  TypoPhone: {
    '& span': {
      fontSize: '1.5rem',
      fontWeight: theme.typography.fontWeightMedium,
      color: deepOrange[800],
    },
  },
}));
