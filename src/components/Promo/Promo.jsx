import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { Container, Typography } from '@material-ui/core';
import useStyles from './styles';

function Promo() {
  const classes = useStyles();

  return (
    <Container maxWidth='xs' className={classes.Container}>
      <Grid container direction='column' align='center'>
        <Grid item>
          <Box className={classes.Box}>
            <img src={'assets/images/promo/promo-20.jpg'} />
          </Box>
        </Grid>
        <Grid item>
          <Typography variant='caption'>Скидка действует на все блюда</Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

export default Promo;
