import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Container: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),

    [theme.breakpoints.down('sm')]: {
      paddingLeft: 0,
      paddingRight: 0,
    },
  },
  Box: {
    width: 200,
    height: 200,
    '& img': {
        maxWidth: '100%'
    }
  },
  Card: {
    display: 'flex',
    justifyContent: 'center',
  },
}));
