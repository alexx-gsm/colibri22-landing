import React from 'react'
import Iframe from 'react-iframe'

function Map() {
  return (
    <Iframe
      url='https://yandex.ru/map-widget/v1/?um=constructor%3A24ead6a0b6215811f80ba1dab102bf06b8e5a9fd6f33f011386c681d519ad428&amp;source=constructor'
      width='100%'
      height='400px'
      id='yandexMap'
      className='yandex-map'
      display='initial'
      position='relative'
      frameBorder='0'
    />
  )
}

export default Map
