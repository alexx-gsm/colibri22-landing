import React from 'react'
import { Container, AppBar, Toolbar, Hidden } from '@material-ui/core'
import { Typography } from '@material-ui/core'
import useStyles from './styles'
import Grid from '@material-ui/core/Grid'

export default function TopPanel({ auth = true }) {
  const classes = useStyles()
  // const [anchorEl, setAnchorEl] = React.useState(null)
  // const open = Boolean(anchorEl)
  //
  // function handleMenu(event) {
  //   setAnchorEl(event.currentTarget)
  // }
  //
  // function handleClose() {
  //   setAnchorEl(null)
  // }

  return (
    <AppBar position='fixed' color='secondary'>
      <Container maxWidth='lg'>
        <Toolbar>
          {/*<Hidden smUp>*/}
          {/*  <IconButton*/}
          {/*    edge='start'*/}
          {/*    className={classes.menuButton}*/}
          {/*    color='inherit'*/}
          {/*    aria-label='Menu'*/}
          {/*  >*/}
          {/*    <MenuIcon />*/}
          {/*  </IconButton>*/}
          {/*</Hidden>*/}
          <Typography variant='h6' className={classes.title}>
            Чайхана "Учкудук"
          </Typography>
          <Grid container spacing={1} alignItems='baseline' justify='flex-end'>
            <Grid item>
              <Hidden smDown>
                <Typography>заказ обедов: </Typography>
              </Hidden>
            </Grid>
            <Grid item>
              <Typography variant='h4'>8-983-170-0471</Typography>
            </Grid>
          </Grid>

          {/*{auth && (*/}
          {/*  <div>*/}
          {/*    <IconButton*/}
          {/*      aria-owns={open ? "__menu-appbar" : undefined}*/}
          {/*      aria-haspopup="true"*/}
          {/*      onClick={handleMenu}*/}
          {/*      color="inherit"*/}
          {/*    >*/}
          {/*      <AccountCircle />*/}
          {/*    </IconButton>*/}
          {/*    <Menu*/}
          {/*      id="__menu-appbar"*/}
          {/*      anchorEl={anchorEl}*/}
          {/*      anchorOrigin={{*/}
          {/*        vertical: "top",*/}
          {/*        horizontal: "right",*/}
          {/*      }}*/}
          {/*      transformOrigin={{*/}
          {/*        vertical: "top",*/}
          {/*        horizontal: "right",*/}
          {/*      }}*/}
          {/*      open={open}*/}
          {/*      onClose={handleClose}*/}
          {/*    >*/}
          {/*      <MenuItem onClick={handleClose}>Profile</MenuItem>*/}
          {/*      <MenuItem onClick={handleClose}>My account</MenuItem>*/}
          {/*    </Menu>*/}
          {/*  </div>*/}
          {/*)}*/}
        </Toolbar>
      </Container>
    </AppBar>
  )
}
